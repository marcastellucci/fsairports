<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<!-- Required meta tags for Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>FSAirports</title>
	<link rel="stylesheet" type="text/css" href="<? echo base_url('js/bootstrap/css/bootstrap.css')?>" >
    <link rel="stylesheet" href="<?= base_url('js/openlayers/v6.4.3-dist/ol.css'); ?>" type="text/css">    
	<style>
	    .map {
	      height: 720px;
	      width: 100%;
	    }

	    .boton_exclude{
	    	background-color: #FF2424;
	    	color:#FFFFFF;
	    }
	</style>    
  
</head>
<body>

<h1>FS Airports</h1>

	<div id="map" class="map"></div>
	<div id="mouse-position"></div>
	<form class="form-inline">
      <label>Geometry type &nbsp;</label>
      <select id="type">
        <option value="Point">Point</option>
        <option value="LineString">LineString</option>
        <option value="Polygon">Polygon</option>
        <option value="Circle">Circle</option>
        <option value="None">None</option>
      </select>
    </form>
	 <div class="form-check">
	    <input type="checkbox" class="form-check-input" id="chk_modificar">
	    <label class="form-check-label" for="chk_modificar">Modificar</label>
	 </div>

	 <button type="button" id="btn_vectores" class="btn btn-primary">VECTORES</button>
	 <button type="button" id="btn_exclude" class="btn btn-warning">Exclude</button>
	 <button type="button" id="btn_exclude_on" class="btn btn-danger">Exclude</button>


	<script src="<?= base_url('js/jquery/jquery-3.5.1.min.js');?>"></script>
	<script src="<?= base_url('js/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script src="<?= base_url('js/openlayers/v6.4.3-dist/ol.js'); ?>"></script>  		

	<script src="<?= base_url('js/map_styles.js'); ?>"></script>  		
	<script src="<?= base_url('js/map.js'); ?>"></script>  		


	<script type="text/javascript">
		var map;
		var mody;	
		$('#btn_exclude_on').hide();
	

		$(document).ready(function() {		

			$('#chk_modificar').change(function(){		
				Modificar( $('#chk_modificar').prop('checked') );
			});

			$('#btn_exclude').click(function(){
				$('#btn_exclude').hide();
				$('#btn_exclude_on').show();
			});
			$('#btn_exclude_on').click(function(){
				$('#btn_exclude_on').hide();
				$('#btn_exclude').show();
			});


	        function Modificar(onoff) {

	        	if (onoff==true){	        		  
			        	mody=new ol.interaction.Modify({				          
							          source: vector_poly
							    });

					  map.addInteraction(mody);
		        }else{		        	
		        	map.removeInteraction(mody);
		        }
	        }


    	});

	</script>	
</body>
</html>