<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<!-- Required meta tags for Bootstrap -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>FSAirports</title>
	<link rel="stylesheet" type="text/css" href="<? echo base_url('js/bootstrap/css/bootstrap.css')?>" >
  <link rel="stylesheet" href="<?= base_url('js/openlayers/v6.4.3-dist/ol.css'); ?>" type="text/css">    
  <link rel="stylesheet" href="<?= base_url('css/sidebar.css'); ?>" type="text/css">    
  <link href="<? echo base_url('js/bootstrap/css/bootstrap-toggle.min.css')?>" rel="stylesheet">

	<style>
	    .map {
	      height: 800px;
	      width: 100%;
	    }

	    .boton_exclude{
	    	background-color: #FF2424;
	    	color:#FFFFFF;
	    }

	</style>    
  
</head>
<body>
  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading">FSAirports</div>
      <div class="list-group list-group-flush">
        <a href="#" class="list-group-item list-group-item-action bg-light">          
          <div class="checkbox">
          <label>
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" data-toggle="toggle" id="toggle_exclude" data-on="Exclude&nbsp;&nbsp;" data-off="Exclude&nbsp;&nbsp;" data-onstyle="primary" data-offstyle="info">            
          </label>
        </div> </a>
        <a href="#" class="list-group-item list-group-item-action bg-light">
          <div class="checkbox">
          <label>
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" data-toggle="toggle" id="toggle_aprons" data-on="Apron&nbsp;&nbsp;" data-off="Apron&nbsp;&nbsp;" data-onstyle="primary" data-offstyle="info">            
          </label>
        </div>           
        </a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Taxi</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Events</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">          
          <input type="button" name="btn_geometry" id="btn_geometry" class="btn btn-warning" value="get Geometry">
        </a>
        <a href="#" class="list-group-item list-group-item-action bg-light">
          <div class="checkbox">
          <label>
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" data-toggle="toggle" id="toggle_modify" data-on="Modify" data-off="Modify &nbsp;&nbsp;&nbsp;" data-onstyle="primary" data-offstyle="info">            
          </label>
        </div>        
        </a>
        <a href="#" class="list-group-item list-group-item-action bg-light">
        <div class="checkbox">
          <label>
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" data-toggle="toggle" id="toggle_snap" data-on="Snap" data-off="Snap &nbsp;&nbsp;&nbsp;" data-onstyle="primary" data-offstyle="info">            
          </label>
        </div>        
        </a>
        <a href="#" class="list-group-item list-group-item-action bg-light">          
          <input type="button" name="btn_compilar" id="btn_compilar" class="btn btn-success" value="COMPILE">
        </a>        
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Ocultar</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container-fluid">
        <div id="map" class="map"></div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->
<!-- 
	<div id="mouse-position"></div>
	<form class="form-inline">
      <label>Geometry type &nbsp;</label>
      <select id="type">
        <option value="Point">Point</option>
        <option value="LineString">LineString</option>
        <option value="Polygon">Polygon</option>
        <option value="Circle">Circle</option>
        <option value="None">None</option>
      </select>
    </form>
	 <div class="form-check">
	    <input type="checkbox" class="form-check-input" id="chk_modificar">
	    <label class="form-check-label" for="chk_modificar">Modificar</label>
	 </div>

	 <button type="button" id="btn_vectores" class="btn btn-primary">VECTORES</button>
	 <button type="button" id="btn_exclude" class="btn btn-warning">Exclude</button>
	 <button type="button" id="btn_exclude_on" class="btn btn-danger">Exclude</button> -->


	<script src="<?= base_url('js/jquery/jquery-3.5.1.min.js');?>"></script>
	<script src="<?= base_url('js/bootstrap/js/bootstrap.min.js'); ?>"></script>
  <script src="<?= base_url('js/bootstrap/js/bootstrap-toggle.min.js');?>"></script>
	<script src="<?= base_url('js/openlayers/v6.4.3-dist/ol.js'); ?>"></script>  		

	<script src="<?= base_url('js/map_styles.js'); ?>"></script>  		
	<script src="<?= base_url('js/map.js'); ?>"></script>  		


	<script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';
    var map;
    var layerbase;
    var source_poly;
    var vectorSource;
    var vectorLayer;
    var vector_draw;

		var modify_feature;    
    var draw_feature;	    
    var snap_feature;

    var drawing=false;
    var modifying=false;
    var snapping=false;
    

    draw_feature = new ol.interaction.Draw({
              type: 'Polygon',
              source: source_poly
        });
    modify_feature=new ol.interaction.Modify({                  
                        source: source_poly
         });

    snap_fature = new ol.interaction.Snap({ source: source_poly });
    

		$('#btn_exclude_on').hide();
	

		$(document).ready(function() {	

      $('#menu-toggle').click(function(e){
        e.preventDefault();
        if (document.getElementById('toggle-menu').html=="Ocultar"){
          document.getElementById('toggle-menu').innerHTML = 'Mostrar';
        }
        $('#wrapper').toggleClass("toggled");
      });

			$('#btn_exclude').click(function(){
				$('#btn_exclude').hide();
				$('#btn_exclude_on').show();
			});
			$('#btn_exclude_on').click(function(){
				$('#btn_exclude_on').hide();
				$('#btn_exclude').show();
			});

      //--------------------------------------------------------
      //  APRONS -  activar o desactivar DRAW_FEATURE
      //--------------------------------------------------------
      $('#toggle_aprons').change(function(e){
            e.preventDefault();
            drawing = !drawing;
           if (drawing){
              map.addInteraction(draw_feature);
            }else{
              map.removeInteraction(draw_feature);
           }
       });

      //--------------------------------------------------------
      //  MODIFICAR -  activar o desactivar MODIFY_FEATURE
      //--------------------------------------------------------
       $('#toggle_modify').change(function(){ 
          
          modifying = !modifying;

          if (modifying==true){
             map.addInteraction(modify_feature);
          }else{              
             map.removeInteraction(modify_feature);
          }
        });

      //--------------------------------------------------------
      //  SNAP -  activar o desactivar SNAPPING
      //--------------------------------------------------------
      $('#toggle_snap').change(function(){
          snapping = !snapping;

          // if (modifying){
          //   $('#toggle_modify').bootstrapToggle("off");
          // }

          // if ($('#chk_modificar').prop('checked')){
          if (snapping){
             map.addInteraction(snap_fature);             
          }else{
             map.removeInteraction(snap_fature);
          }
      });


      //-------------------------------------------------------
      //   COMPILAR  -  .get JQUERY ajax
      //-------------------------------------------------------
      $('#btn_compilar').click(function(e){

          var url=base_url+"index.php/home/compilar";
          var params={
                      archivo:"c:/ide/compilar.xml"
                      };
          console.log(url+"?archivo=c:/ide/compilar.xml");
          console.log(params);
          $.get(url,
                params
          ,function(output){               
                if (output!=null){
                   console.log(output);
                }else{
                  console.log("vacio");
                }
          },"json");
      });
      //-------------------------------------------------------

      $('#btn_geometry').click(function(e){

          var src = 'EPSG:3857';
          var dest = 'EPSG:4326';

          var features = source_poly.getFeatures();
          console.log("FEATURES=");
          console.log(features.length);

          for(var f=0; f< features.length; f++) {
              // source_poly.removeFeature(features[i]);
              // var geom = features[i].getGeometry();
              var geom = features[f].getGeometry().transform(src, dest);

              var coords=geom.getCoordinates();

              console.log("FEATURE="+f+"   vertices="+coords[0].length);

              for (i=0;i<coords[0].length;i++){
                console.log(coords[0][i][0]+' / '+coords[0][i][1]);
              }

          }       

      });

      });

      function parse_geometry(un_vector,tipo){
          
          var features = un_vector.features;    
          console.log(features);
          var geometria="";
          if (features.length>0){
              for (var i = 0; i < features.length; i++) {
                      var f = features[i];
                      if (i==0){
                          geometria+=serialize_features(f,tipo);
                      }else{
                          geometria+=", "+serialize_features(f,tipo);
                      }
                  }
              // para salida GeoJSON habilitar:
              // geometria='{"type": "FeatureCollection","features": ['+geometria+']}'; 
          }
          // retorna un WellKnowText
          return geometria;

      }

	</script>	
</body>
</html>