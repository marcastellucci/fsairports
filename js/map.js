  
   

      var styleFunction = function(feature) {
        return styles[feature.getGeometry().getType()];
      };


 
  // ------Capa MAPA SATELITAL --------------
  var ESRI_satellite = new ol.source.XYZ({
              attributions: ['Powered by Esri',
                             'Source: Esri, DigitalGlobe, GeoEye, Earthstar Geographics, CNES/Airbus DS, USDA, USGS, AeroGRID, IGN, and the GIS User Community'],
              attributionsCollapsible: false,
              url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
              maxZoom: 23
            });

  // ------Capa MAPA OPENSTREETMAP --------------
  var OpenStreetMap = new ol.source.OSM();

      var source_poly = new ol.source.Vector({wrapX: false});

      var vector_draw = new ol.layer.Vector({
        source: source_poly
      });

    var mousePositionControl = new ol.control.MousePosition({
      coordinateFormat: ol.coordinate.createStringXY(4),
      projection: 'EPSG:4326',
      // comment the following two lines to have the mouse position
      // be placed within the map.
      className: 'custom-mouse-position',
      target: document.getElementById('mouse-position'),
      undefinedHTML: '&nbsp;'
    });

  var layerbase=new ol.layer.Tile({
              source: ESRI_satellite
            });

  map = new ol.Map({
    target: 'map',
    controls: ol.control.defaults().extend([mousePositionControl]),
    layers: [
            layerbase,            
            vector_draw,
          ],
    view: new ol.View({
      center: ol.proj.fromLonLat([-68.528125,-31.541580]),
      zoom: 6
    })
  });	


  // -------- DRAW POLY -----------

  var typeSelect = document.getElementById('type');



  // map.addInteraction(new ol.interaction.Modify({
  //   source: vector_poly
  // }));

  // map.addInteraction(new ol.interaction.Snap({
  //   source: vector_poly
  // }));

   // var precisionInput = 6; // document.getElementById('precision');
   //   precisionInput.addEventListener('change', function(event) {
   //    var format = createStringXY(event.target.valueAsNumber);
   //    mousePositionControl.setCoordinateFormat(format);
   //  });