
      var vector_poly = new ol.source.Vector({wrapX: false});

      var draw_vector = new ol.layer.Vector({
        source: vector_poly
      });


      var typeSelect = document.getElementById('type');

      var draw; // global so we can remove it later
      function addInteraction() {
        var value = typeSelect.value;
        if (value !== 'None') {
          draw = new ol.interaction.Draw({
            source: draw_vector,
            type: 'Polygon'
          });
          map.addInteraction(draw);
        }
      }


      
      /**
       * Handle change event.
       */
      typeSelect.onchange = function() {
        map.removeInteraction(draw);
        addInteraction();
      };

      addInteraction();